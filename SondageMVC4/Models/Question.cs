namespace SondageMVC4.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Question")]
    public partial class Question
    {
        public Question()
        {
            Reponses = new HashSet<Reponse>();
        }

        public Guid Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Text { get; set; }

        public Guid Questionnaire_Id { get; set; }

        public virtual Questionnaire Questionnaire { get; set; }

        public virtual ICollection<Reponse> Reponses { get; set; }
    }
}
