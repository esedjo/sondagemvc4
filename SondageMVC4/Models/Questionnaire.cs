namespace SondageMVC4.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Questionnaire")]
    public partial class Questionnaire
    {
        public Questionnaire()
        {
            Questions = new HashSet<Question>();
        }

        public Guid Id { get; set; }

        [Required]
        [StringLength(80)]
        public string Titre { get; set; }

        [Column(TypeName = "text")]
        public string Description { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DateCloture { get; set; }

        [Required]
        [StringLength(50)]
        public string Hash { get; set; }

        public Guid Admin { get; set; }

        public virtual ICollection<Question> Questions { get; set; }

        public virtual User User { get; set; }
    }
}
