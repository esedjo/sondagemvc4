namespace SondageMVC4.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Reponse")]
    public partial class Reponse
    {
        public Reponse()
        {
            User_Reponse = new HashSet<User_Reponse>();
        }

        public Guid Id { get; set; }

        [Required]
        [StringLength(70)]
        public string Text { get; set; }

        public Guid Question_Id { get; set; }

        public virtual Question Question { get; set; }

        public virtual ICollection<User_Reponse> User_Reponse { get; set; }
    }
}
