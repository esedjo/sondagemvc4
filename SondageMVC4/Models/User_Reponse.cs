namespace SondageMVC4.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class User_Reponse
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        public Guid User_Id { get; set; }

        public Guid Reponse_Id { get; set; }

        public virtual Reponse Reponse { get; set; }

        public virtual User User { get; set; }
    }
}
